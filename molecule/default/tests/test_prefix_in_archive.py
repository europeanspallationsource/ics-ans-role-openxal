import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('prefix_in_archive')

OPENXAL_VERSION = '1.1.1'


def test_openxal_installed(host):
    assert host.file('/opt/openxal-{0}/lib/openxal.library-{0}.jar'.format(OPENXAL_VERSION)).exists


def test_previous_install_removed(host):
    assert not host.file('/opt/openxal-1.0.0').exists
