import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('default_group')

OPENXAL_VERSION = '1.2.2'


def test_openxal_link(host):
    openxal_link = host.file('/opt/OpenXAL')
    assert openxal_link.is_symlink
    assert openxal_link.linked_to == '/opt/openxal-{}'.format(OPENXAL_VERSION)


def test_openxal_installed(host):
    assert host.file('/opt/openxal-{0}/lib/openxal.library-{0}.jar'.format(OPENXAL_VERSION)).exists
    assert host.file('/opt/openxal-{0}/apps/openxal.apps.launcher-{0}.jar'.format(OPENXAL_VERSION)).exists


def test_openxal_preferences(host):
    prefs = host.file('/etc/openxal/xal.smf.data.prefs')
    assert prefs.content_string.strip() == 'mainPath=/opt/openxal-{}/optics/design/main.xal'.format(OPENXAL_VERSION)
