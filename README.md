ics-ans-role-openxal
====================

Ansible role to install OpenXAL applications.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------

```yaml
openxal_version: 1.2.2
openxal_archive: "https://artifactory.esss.lu.se/artifactory/OpenXAL/org/xal/openxal.dist/{{ openxal_version }}/openxal.dist-{{ openxal_version }}-dist.tar.gz"
# set to true to remove previous installations (keep only current version)
openxal_clean_previous_install: false
openxal_use_rbac: "false"
```


Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-openxal
```

License
-------

BSD 2-clause
